package com.dmitriy.azarenko.recyclerview_secactiv_click;

import android.view.View;

/**
 * Created by Дмитрий on 09.02.2016.
 */
public abstract class OnItemClickWatcher<T> {


    public abstract void onItemClick(View v, int position, T item);
}
