package com.dmitriy.azarenko.recyclerview_secactiv_click;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        TextView namesTv = (TextView)findViewById(R.id.nameIdSec);
        TextView emailTv = (TextView)findViewById(R.id.emailIdSec);
        TextView adressTv = (TextView)findViewById(R.id.adressIDSec);
        TextView phoneTv = (TextView)findViewById(R.id.phoneIDSec);
        ImageView imageTv = (ImageView) findViewById(R.id.imageViewSec);

       Names namesForSec = (Names) getIntent().getExtras().getSerializable("Key");

        namesTv.setText(namesForSec.getName());
        emailTv.setText(namesForSec.getEmail());
        adressTv.setText(namesForSec.getAdress());
        phoneTv.setText(namesForSec.getPhone());
        imageTv.setImageResource(R.drawable.ic_man);


    }

}
