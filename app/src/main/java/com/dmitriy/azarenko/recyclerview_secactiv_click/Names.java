package com.dmitriy.azarenko.recyclerview_secactiv_click;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Дмитрий on 09.02.2016.
 */
public class Names implements Serializable {
    String name;
    String eMail;
    String adress;
    String phone;
    int image;

    public Names(String name, String eMail, String adress, String phone,int image) {
        this.name = name;
        this.eMail = eMail;
        this.adress = adress;
        this.phone = phone;
        this.image = image;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){

        this.name = name;
    }
    public String getEmail(){
        return eMail;
    }
    public void setEmail(String eMail){

        this.eMail = eMail;
    }
    public String getAdress(){
        return adress;
    }
    public void setAdress(String adress){

        this.adress = adress;
    }
    public String getPhone(){
        return phone;
    }
    public void setPhone(String phone){

        this.phone = phone;
    }
    public int getImage(){
        return image;
    }
    public void setImage(int image){
        this.image = image;
    }

}
