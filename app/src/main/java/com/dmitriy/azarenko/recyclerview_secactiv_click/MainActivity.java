package com.dmitriy.azarenko.recyclerview_secactiv_click;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

public class MainActivity extends AppCompatActivity {

    public ArrayList<Names> names = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        for (int i = 0; i < 10;i++){

            names.add(new Names("Valera"+ i, "Valera@mail","adress of Valera","055335533",R.drawable.ic_phone));
        }
        RecyclerView list = (RecyclerView) findViewById(R.id.listView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        list.setAdapter(new RecyclerViewAdapter(names, new OnItemClickWatcher<Names>() {
            @Override
            public void onItemClick(View v, int position, Names item) {
                Intent mintent = new Intent(MainActivity.this,SecondActivity.class);
                Names namesForSec = names.get(position);
                mintent.putExtra("Key", namesForSec);
                startActivity(mintent);
            }
        }));



    }




    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> implements View.OnClickListener {

        private OnItemClickWatcher<Names> watcher;
        ArrayList<Names> names;

        public RecyclerViewAdapter(ArrayList<Names> names, OnItemClickWatcher<Names> watcher){
            this.names = names;
            this.watcher = watcher;
        }



        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
            View v = getLayoutInflater().inflate(R.layout.list_item, parent,false);
            return new ViewHolder(v, watcher, names);

        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            holder.name.setText(names.get(i).getName());
            holder.eMail.setText(names.get(i).getEmail());
            holder.image.setImageResource(names.get(i).getImage());

        }

        @Override
        public int getItemCount() {
            return names.size();
        }

        @Override
        public void onClick(View v) {

        }


        class ViewHolder extends RecyclerView.ViewHolder{
            private TextView name;
            private TextView eMail;
            private ImageView image;

            public ViewHolder(View itemView, final OnItemClickWatcher<Names> watcher, final ArrayList<Names> names) {
                super(itemView);
                name = (TextView) itemView.findViewById(R.id.nameId);
                eMail = (TextView) itemView.findViewById(R.id.emailId);
                image = (ImageView) itemView.findViewById(R.id.imageView);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        watcher.onItemClick(v, getAdapterPosition(), names.get(getAdapterPosition()));
                    }
                });
            }
        }
    }



}